package com.muhardin.endy.training.microservice.nasabahservice.dao;

import com.muhardin.endy.training.microservice.nasabahservice.entity.Nasabah;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface NasabahDao extends PagingAndSortingRepository<Nasabah,Long> {

}