package com.muhardin.endy.training.microservice.nasabahservice.controller;

import com.muhardin.endy.training.microservice.nasabahservice.dao.NasabahDao;
import com.muhardin.endy.training.microservice.nasabahservice.entity.Nasabah;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NasabahController {

    @Autowired private NasabahDao nasabahDao;

    @GetMapping("/nasabah/")
    public Page<Nasabah> ambilDataNasabah(Pageable page){
        return nasabahDao.findAll(page);
    }
}